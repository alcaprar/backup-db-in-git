#!/bin/bash
export BASE_DIR=$(cd `dirname $0` && pwd)

CONTAINER_NAME="${RANDOM}${RANDOM}"

docker build -t backup-db-in-git .
docker run -v $BASE_DIR:/app/database --env-file $BASE_DIR/sqlite.env --name $CONTAINER_NAME backup-db-in-git

CONTAINER_STATUS=$(docker inspect $CONTAINER_NAME --format='{{.State.ExitCode}}')

echo "Container with name '${CONTAINER_NAME}' finished with status '${CONTAINER_STATUS}'"