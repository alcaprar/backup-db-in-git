FROM alpine:3.15

RUN apk add --no-cache bash
RUN apk add --no-cache git
RUN apk add --update sqlite
RUN apk add gettext 
RUN apk add --update postgresql

# copy crontabs for root user
COPY cronjobs /app/cronjobs

WORKDIR /app

RUN mkdir database

COPY src ./src

RUN mv /app/src/docker_cmd.sh /usr/local/bin

CMD ["docker_cmd.sh"]