#!/bin/bash
set -x
set -e # the script will fail if a command fails

$BASE_DIR/fail_if_variable_not_set.sh GIT_REPO_NAME

echo "Pushing the dump.."
(
    cd $REPO_PATH &&
    git status &&
    git add . &&
    git commit -m "Update db." &&
    git push    
)
echo " Pushed to '${GIT_REPO_NAME}'"