DUMP_FILE_DIR="${DUMP_FILE}-parts"
mkdir -p $DUMP_FILE_DIR

LINES=250
echo "[main.sh] splitting the file into smaller chunks"
split -l $LINES $DUMP_FILE "${DUMP_FILE_DIR}/${TARGET_FILE}."

echo "[main.sh] removing non-splitted file."
rm $DUMP_FILE
