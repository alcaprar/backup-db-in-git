#!/bin/bash
set -x
VARIABLE_NAME=$1

if [[ -z ${!VARIABLE_NAME+x} ]]; then
    echo "Variable '${VARIABLE_NAME} is unset.'"
    exit 1
fi