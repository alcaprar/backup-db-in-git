#!/bin/bash
echo "[git_clone.sh] - START -"

$BASE_DIR/fail_if_variable_not_set.sh GIT_USERNAME
$BASE_DIR/fail_if_variable_not_set.sh GIT_PASSWORD
$BASE_DIR/fail_if_variable_not_set.sh GIT_OWNER

GIT_REPO_URL="https://${GIT_USERNAME}:${GIT_PASSWORD}@github.com/${GIT_OWNER}/${GIT_REPO_NAME}.git"

echo "[git_clone.sh] Checking if repo was already cloned..."
if [[ -d "$REPO_PATH" ]]
then
    echo "[git_clone.sh] -Repository already exists."
    echo "[git_clone.sh] --Updating it..."
    (cd $REPO_PATH && git pull)
    echo "[git_clone.sh] ---Local repo updated."
else
    echo "[git_clone.sh] Cloning the repository..."
    git clone $GIT_REPO_URL $REPO_PATH
    echo "[git_clone.sh] -Repository cloned."
fi

echo "[git_clone.sh] -- END --"