#!/bin/bash
set -x
set -e # the script will fail if a command fails

$BASE_DIR/fail_if_variable_not_set.sh DB_NAME
$BASE_DIR/fail_if_variable_not_set.sh GIT_REPO_NAME
TEMP_DB_NAME="/app/${DB_NAME}.tmp"
TEMP_DUMP_FILE="${TEMP_DB_NAME}.dump.sql"

DB_PATH="/app/database/${DB_NAME}"

if [ ! -f "$DB_PATH" ]; then
    echo "$DB_PATH does not exist."
    exit 1
fi

mkdir -p /dump

echo "===Listing current folder==="
ls
echo "============================"

echo "Copying '${DB_PATH}' in a temp folder."
cp $DB_PATH $TEMP_DB_NAME
echo "  File moved here: '${TEMP_DB_NAME}'"

echo "Running DB dump.."
sqlite3 $TEMP_DB_NAME .dump > $TEMP_DUMP_FILE
echo "  Dump finished."

(cd `dirname $TEMP_DUMP_FILE` && ls)

echo "Moving the tmp dump into the repo.."
mv $TEMP_DUMP_FILE $DUMP_FILE
echo "  File moved."

rm $TEMP_DB_NAME