#!/bin/bash
set -e # the script will fail if a command fails

CRON=${CRON:-false}
DEFAULT_CRON_EXPRESSION="* * * * *"

git config --global user.email "backupper@git.com"
git config --global user.name "DB Backupper"

if $CRON; then
    echo "Starting cron process."
    echo "CRON ${CRON_EXPRESSION}"
    export CRON_EXPRESSION=${CRON_EXPRESSION:-$DEFAULT_CRON_EXPRESSION}
    envsubst < /app/cronjobs > /etc/crontabs/root
    echo "===crontabs content: ==="
    cat /etc/crontabs/root
    echo "========================"
    crond -f -d 8
else 
    echo "Run onetime script."
    /app/src/main.sh
    echo "  Finished."
fi