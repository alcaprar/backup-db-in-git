#!/bin/bash
echo "[main].sh - START -"

now=$(date)
echo "[main.sh] Start time: ${now}"

export BASE_DIR=$(cd `dirname $0` && pwd)
echo "[main.sh] BASE_DIR=${BASE_DIR}"

$BASE_DIR/fail_if_variable_not_set.sh GIT_REPO_NAME
export REPO_PATH="/app/${GIT_REPO_NAME}"
echo "[main.sh] REPO_PATH=${REPO_PATH}"

# Increase git timeout values - https://stackoverflow.com/questions/58961697/i-o-timeout-when-pushing-to-a-git-reporsitory
git config lfs.dialtimeout 3600
git config lfs.activitytimeout 3600

$BASE_DIR/git_clone.sh

$BASE_DIR/fail_if_variable_not_set.sh DB_TYPE

TARGET_FILE=${TARGET_FILE:-$DB_NAME}
export DUMP_FILE="${REPO_PATH}/${TARGET_FILE}.sql"

echo "[main.sh] DB_TYPE=${DB_TYPE}"
case $DB_TYPE in
  sqlite)
    echo "[main.sh] Running sqlite script."
    $BASE_DIR/dump_sqlite.sh
    ;;

  postgres | pg | postgresql)
    echo "[main.sh] Running postgres script."
    $BASE_DIR/dump_postgres.sh
    ;;
  *)
    echo "[main.sh] DB_TYPE '${DB_TYPE}' not supported."
    exit 1
    ;;
esac

$BASE_DIR/split_file.sh

$BASE_DIR/git_push.sh

echo "[main.sh] - END -"
exit 0