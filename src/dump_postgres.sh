#!/bin/bash
set -e # the script will fail if a command fails

DB_NAME=${DB_NAME:-postgres}
DB_USERNAME=${DB_USERNAME:-postgres}
DB_PASSWORD=${DB_PASSWORD:-password}
DB_HOST=${DB_HOST:-localhost}
DB_PORT=${DB_PORT:-5432}
SCHEMA_NAME=${DB_SCHEMA_NAME:-public}

RETRIES=5

until psql -h $DB_HOST -p $DB_PORT -U $DB_USERNAME -d $DB_NAME -c "select 1" > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "[dump_postgres.sh] Waiting for postgres server, $((RETRIES--)) remaining attempts..."
  sleep 1
done

echo "[dump_postgres.sh] Running DB dump.."
PGPASSWORD="${DB_PASSWORD}" pg_dump -U $DB_USERNAME -h $DB_HOST -p $DB_PORT -n $SCHEMA_NAME -d $DB_NAME > $DUMP_FILE
echo "[dump_postgres.sh] - Dump finished."