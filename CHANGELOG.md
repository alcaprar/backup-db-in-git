# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.35](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.34...v0.1.35) (2022-04-02)


### Bug Fixes

* increase git timeout values ([743dff3](https://gitlab.com/alcaprar/backup-db-in-git/commit/743dff399440625e26a1e81a01137c3311faa17f))

### [0.1.34](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.33...v0.1.34) (2022-04-02)


### Bug Fixes

* increase lines per file to 250 ([9b9831b](https://gitlab.com/alcaprar/backup-db-in-git/commit/9b9831bb092f3b7b4029fc48ea1b216f4dc42eae))

### [0.1.33](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.32...v0.1.33) (2022-04-02)


### Bug Fixes

* pull repo if it is already cloned ([f7bd801](https://gitlab.com/alcaprar/backup-db-in-git/commit/f7bd8012eec11fc5383c49f2f9cd83bd8de80f50))

### [0.1.32](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.31...v0.1.32) (2022-04-02)


### Features

* split file into smaller chunks before pushing ([6116cf7](https://gitlab.com/alcaprar/backup-db-in-git/commit/6116cf7143602ad49b1f210fba0ab2c02e013208))

### [0.1.31](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.30...v0.1.31) (2022-01-02)


### Features

* postgres support draft ([bfd6d6c](https://gitlab.com/alcaprar/backup-db-in-git/commit/bfd6d6c72bce1595cd03a7ae2326036f0bde3f22))

### [0.1.30](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.29...v0.1.30) (2022-01-01)

### [0.1.29](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.28...v0.1.29) (2022-01-01)


### Bug Fixes

* print datetime when starting ([b4bf3d1](https://gitlab.com/alcaprar/backup-db-in-git/commit/b4bf3d179e8ee876a22e2e48f9bfc3044655fca6))

### [0.1.28](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.27...v0.1.28) (2022-01-01)


### Bug Fixes

* finally cron works ([5a20680](https://gitlab.com/alcaprar/backup-db-in-git/commit/5a2068088f525a3aa28fd72004acde3182ef8c38))

### [0.1.27](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.26...v0.1.27) (2021-12-29)


### Features

* tag latest docker with latest tag ([eff7d82](https://gitlab.com/alcaprar/backup-db-in-git/commit/eff7d8273e6e0f4561da61ac3cc1b1c432388e0b))

### [0.1.26](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.25...v0.1.26) (2021-12-29)


### Bug Fixes

* always clone the repo and not pull ([7db76b8](https://gitlab.com/alcaprar/backup-db-in-git/commit/7db76b8779faded9ff6debb22a3645b5900ff1a3))

### [0.1.25](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.24...v0.1.25) (2021-12-27)


### Bug Fixes

* add default crontab to every minute ([d097596](https://gitlab.com/alcaprar/backup-db-in-git/commit/d0975964aa0003282fd36b095fb128647e06ca01))

### [0.1.24](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.23...v0.1.24) (2021-12-27)


### Features

* possibility to change crontab at runtime ([13868b0](https://gitlab.com/alcaprar/backup-db-in-git/commit/13868b0916cd96d393e2b80d522ab1abdb1a55f7))

### [0.1.23](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.22...v0.1.23) (2021-12-27)


### Bug Fixes

* use the correct script in cronjob ([cbdef9b](https://gitlab.com/alcaprar/backup-db-in-git/commit/cbdef9b0f41cbd29f21c8563c0833349411130b5))

### [0.1.22](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.21...v0.1.22) (2021-12-27)


### Features

* start crond when CRON is set to true ([16a5598](https://gitlab.com/alcaprar/backup-db-in-git/commit/16a5598b9a0372ac179e6fbe0c008ec2259cf5f2))

### [0.1.21](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.20...v0.1.21) (2021-12-27)


### Bug Fixes

* using always absolute paths ([dcfa478](https://gitlab.com/alcaprar/backup-db-in-git/commit/dcfa47825558e4147ca708ca8ac5f3e0c4c68392))

### [0.1.20](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.19...v0.1.20) (2021-12-27)


### Bug Fixes

* creating the dump in the git folder ([5c263b0](https://gitlab.com/alcaprar/backup-db-in-git/commit/5c263b016190d11ae4e934323d6935cb72f64168))

### [0.1.19](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.18...v0.1.19) (2021-12-27)


### Bug Fixes

* create the dump folder ([347aa72](https://gitlab.com/alcaprar/backup-db-in-git/commit/347aa7212f966b98136c032b96534cf8c07f24e5))
* print git status before pushing ([deea527](https://gitlab.com/alcaprar/backup-db-in-git/commit/deea5274f8d4f36f23c1c137e7d59b60e3c7708e))
* remove useless logs ([bf19e62](https://gitlab.com/alcaprar/backup-db-in-git/commit/bf19e6247bab3e45ac2904fb7c91f1d4501517b3))

### [0.1.18](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.17...v0.1.18) (2021-12-27)


### Features

* push docker with git tag ([ea35041](https://gitlab.com/alcaprar/backup-db-in-git/commit/ea350416a14e1b8c8c50d54b1224cb1728ada17a))

### [0.1.18](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.17...v0.1.18) (2021-12-27)


### Features

* push docker with git tag ([ea35041](https://gitlab.com/alcaprar/backup-db-in-git/commit/ea350416a14e1b8c8c50d54b1224cb1728ada17a))

### [0.1.18](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.17...v0.1.18) (2021-12-27)


### Features

* push docker with git tag ([ea35041](https://gitlab.com/alcaprar/backup-db-in-git/commit/ea350416a14e1b8c8c50d54b1224cb1728ada17a))

### [0.1.18](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.17...v0.1.18) (2021-12-27)

### [0.1.17](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.16...v0.1.17) (2021-12-27)

### [0.1.16](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.15...v0.1.16) (2021-12-27)

### [0.1.15](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.14...v0.1.15) (2021-12-27)

### [0.1.14](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.13...v0.1.14) (2021-12-27)

### [0.1.13](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.12...v0.1.13) (2021-12-27)

### [0.1.12](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.11...v0.1.12) (2021-12-27)

### [0.1.11](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.10...v0.1.11) (2021-12-27)

### [0.1.10](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.9...v0.1.10) (2021-12-27)

### [0.1.9](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.8...v0.1.9) (2021-12-27)

### [0.1.8](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.7...v0.1.8) (2021-12-27)

### [0.1.7](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.6...v0.1.7) (2021-12-27)

### [0.1.6](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.5...v0.1.6) (2021-12-27)

### [0.1.5](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.4...v0.1.5) (2021-12-27)

### [0.1.4](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.3...v0.1.4) (2021-12-27)

### [0.1.3](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.2...v0.1.3) (2021-12-27)

### [0.1.2](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.1...v0.1.2) (2021-12-27)


### Bug Fixes

* add git config in tag job ([d53d6f4](https://gitlab.com/alcaprar/backup-db-in-git/commit/d53d6f43285b1f4b09d80709fb77e80536dce324))
* push the tag generated by standard-version ([74e6b3f](https://gitlab.com/alcaprar/backup-db-in-git/commit/74e6b3f90e3fc39f09d68d05c2d0b4241dd1658b))

### [0.1.1](https://gitlab.com/alcaprar/backup-db-in-git/compare/v0.1.0...v0.1.1) (2021-12-27)


### Bug Fixes

* skip ci when committing release ([426a5e1](https://gitlab.com/alcaprar/backup-db-in-git/commit/426a5e1f9b1456d2591689acc948c318d52f713d))

## 0.1.0 (2021-12-27)


### Bug Fixes

* change db file name variable ([f23eaa6](https://gitlab.com/alcaprar/backup-db-in-git/commit/f23eaa68bfb8267b7361859d616f6eb0e070fa16))
* make dump folder ([77fd9bb](https://gitlab.com/alcaprar/backup-db-in-git/commit/77fd9bbb4c3c987bb5d57a80ccc2096fcc4ba167))
* use a proper docker tag name ([ba36fc8](https://gitlab.com/alcaprar/backup-db-in-git/commit/ba36fc8822df7592c01a19eeb93afb9f6342af6b))
